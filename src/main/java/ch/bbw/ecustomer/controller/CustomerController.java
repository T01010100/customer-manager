package ch.bbw.ecustomer.controller;

import ch.bbw.ecustomer.customer.CustomerDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.logging.Logger;

/**
 * @author Moritz Bolliger
 * @version 04.09.2019 15:12
 * @project Package ch.bbw.ecustomer.controller von Projekt ecustomer
 */
public class CustomerController implements Serializable {
    private static Logger log = Logger.getLogger(CustomerDao.class.getSimpleName());
    private static final long serialVersionUID = 1L;

    // DAO Implementation
    @Autowired //Instanziert im Hintergrund das Objekt / DAO mit dependency injection von Spring
    private CustomerDao customerDao;

    @PostConstruct
    public void init() {
        // customerDao = new CustomerDao();
        log.info("im init");
    }
    @GetMapping("/customers") //url – Link auf /customers in diese Methode
    public String getCustomers(Model model) {
        // List<Customer> customers = null;
        // if (customerDao != null) {
        // customers = customerDao.getCustomers();
        // log.info("getCustomers");
        // }

        // model.addAttribute("customers", customers); // Spring - DI
        model.addAttribute("customers", customerDao.getCustomers()); // Spring - DI
        log.info("hello from Controller - getCustomers");
        return "customers";
    }

}

